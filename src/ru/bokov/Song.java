package ru.bokov;

/**
 * Created by Дмитрий on 26.12.2014.
 */
public class Song {

    private String name;
    private String performer;
    private int length;
    private int year;

    public static void main(String[] args) {
        Song a=new Song("asd","mother",185, 2001);
        Song b=new Song("qwerty","attacker",277,2008);
        System.out.println(a);
        System.out.println(b);
        System.out.println(a.category());
        System.out.println(b.category());
        System.out.println(a.similarities(b));
        System.out.println(a.whosolder(b));
    }

    public Song(String name, String performer, int length, int year) {
        this.name = name;
        this.performer = performer;
        this.length = length;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Трек: " +
                "Название - " + name +
                ", Исполнитель - " + performer +
                ", Длина песни - " + length +
                ", Год выпуска - " + year +
                '}';
    }

    public String category() {
        if (length < 120)
            return ("Продолжительность трека: короткая.");
        if (length < 240 && length > 120)
            return ("Продолжительность трека: средняя.");
        else
            return ("Продолжительность трека: длинная.");
    }


    public String similarities(Song b) {
        if (name==b.getName())
            return ("У объектов одинаковое название.");
        if (performer==b.getPerformer())
            return ("У объектов одинаковые исполнители.");
        if (length==b.getLength())
            return ("Объекты одинаковы по длине.");
        else
            return ("У объектов нет похожих характеристик");
    }

    public String whosolder(Song b) {
        if (year<b.getYear())
            return ("Трек А старше трека B");
        else
            return("Трек В старше трека А");
    }
}
